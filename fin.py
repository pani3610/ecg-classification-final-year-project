"""
van Gent, P. (2016). Analyzing a Discrete Heart Rate Signal Using Python. A tech blog about fun things with Python and embedded electronics.
Retrieved from: http://www.paulvangent.com/2016/03/15/analyzing-a-discrete-heart-rate-signal-using-python-part-1/

Code updated for library upgrades and PQST identification

"""
import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np
import wfdb as wf

data=pd.read_csv('F:/mitcsv/100.csv')
data=data.iloc[:2000,1:2]






fsamp=360
coff1=0.75
mov_avg=data.rolling(int(fsamp*coff1)).mean()
mov_avg=mov_avg.fillna(data.mean())
mov_avg=mov_avg*1.2
coff2=0.15
coff3=0.5
coff4=0.4
mov_median=data.rolling(int(fsamp*coff2),center=True).median()
window=[]
px=[]
rx=[]
qx=[]
sx=[]
tx=[]
for i in range(1,len(data)):
    if(data.values[i][0]<mov_avg.values[i][0] and len(window)<1):
        continue
    elif(data.values[i][0]>mov_avg.values[i][0]):
        window.append(data.values[i][0])

    else:
        maximum=max(window)
        beatposition=i-len(window)+window.index(max(window))
        rx.append(beatposition)
        window=[]
        fs=False
        k=beatposition+1
        while(True):
            if((data.values[k][0]-data.values[k-1][0])*(data.values[k+1][0]-data.values[k][0])<=0):
                sx.append(k)
                l=mov_median[k:int(k+fsamp*coff3)].values.tolist()
                
                tx.append(k+l.index(max(l)))
                
                break
            k+=1
        k=beatposition-1
        while(True):
            if((data.values[k][0]-data.values[k-1][0])*(data.values[k+1][0]-data.values[k][0])<=0):
                qx.append(k)
                
                l=mov_median[max(0,int(k-fsamp*coff4)):k].values.tolist()
                #print(max(0,int(k-fsamp*coff4)))
                m=l[::-1]
                #print(k,l)
                px.append(int(max(0,(int(k-fsamp*coff4)+l.index(max(l)))+k-m.index(max(m)))*0.5))
                break
            k-=1

'''
for i in range(2,len(data)-1):
    if((data.values[i][0]-data.values[i-1][0])*(data.values[i+1][0]-data.values[i][0])<=0):
        extreme.append(i)

        
        
yextreme=[data.values[x][0] for x in extreme]   
'''
py=[data.values[x][0] for x in px]
qy=[data.values[x][0] for x in qx]
ry=[data.values[x][0] for x in rx]
sy=[data.values[x][0] for x in sx]
ty=[data.values[x][0] for x in tx]
rrtime=[]
qrstime=[]
prtime=[]
sttime=[]
for i in range(1,len(rx)):
    rrtime.append(rx[i]-rx[i-1])
for i in range(len(qx)):
    qrstime.append(sx[i]-qx[i])
for i in range(len(px)):
    prtime.append(rx[i]-px[i])
for i in range(len(sx)):
    sttime.append(tx[i]-sx[i])
avgtime=sum(rrtime)/len(rrtime)
avgqrs=sum(qrstime)/len(qrstime)
avgpr=sum(prtime)/len(prtime)
avgst=sum(sttime)/len(sttime)

print("Heart rate in bpm :",60*fsamp/avgtime)
print("Average time period of qrs in seconds :",avgqrs/fsamp)
print("Average time period of pr in seconds :",avgpr/fsamp)
print("Average time period of st in seconds :",avgst/fsamp)





plt.plot(data,color='blue',alpha=0.5)
#plt.plot(mov_median,color='green')
plt.scatter(rx,ry,color='red')
plt.scatter(qx,qy,color='brown')
plt.scatter(sx,sy,color='cyan')
plt.scatter(tx,ty,color='green')
plt.scatter(px,py,color='purple')
#plt.scatter(extreme,yextreme,color='orange')
#plt.show()
'''
plt.figure()
plt.plot(rrtime)
plt.plot([avgtime for x in range(len(rrtime))])
plt.figure()
'''
plt.show()
